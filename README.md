Basic config for Debian
=======================

This script configures a basic configuration for Debian

The following components are configured:

+ Install git and tig
+ Set git username and email
+ Install bash-completion
+ Install and configure auto updates (unattended-upgrades and apt-listchanges
+ Install and tune logrotate
+ Disable IPv6 (or no, see parameters.conf)
+ Tune .vimrc
+ Tune .bashrc
+ Tune .profile
+ Send alert email for new ssh connexion
+ Set Time Zone (see parameters.conf to edit, default "Europe/Paris")

You just need to clone this repository in `/srv/git/basic_config_debian` and to execute the `auto_config.sh` script

```bash
apt install git
mkdir -p /srv/git
git clone https://framagit.org/zorval/config_system/basic_config_debian.git /srv/git/basic_config_debian
cd /srv/git/basic_config_debian
./auto_config.sh
```

## New aliases and available functions

### Hard drive
+ `hddtop`: print hard drive usage

### Memory
+ `memtop`: print top usage memory

### Network
+ `ipa`, `ip4a`, `ip6a` and `ip4all`: print interfaces list

### Systemd / Journald
+ `jf`: alias for `journalctl -f`
+ `jfu <units>`: function for `journalctl -f -u <unit> -u <unit>...`
+ `scf`: alias for `systemctl list-units --failed`
+ `scs <units>`: function for `systemctl status <units>`
+ `scc <units>`: function for `systemctl cat <units>`
