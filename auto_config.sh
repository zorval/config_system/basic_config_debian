#!/bin/bash

# BSD 3-Clause License
# 
# Copyright (c) 2018-2019, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################

# Current git directory
GIT_DIR="$(realpath ${0%/*})"

# Conf file path
CONF_FILE="$GIT_DIR/conf"

# Used if parameter is required - Do not edit
RELOAD_PARAMATER=false

################################################################################

# Source paramaters
. $GIT_DIR/parameters.conf

################################################################################

# Function to request parameters
function func_read_param() {
    # $1 => PARAM => Parameter name
    PARAM="$1"
    # after shitf, $@ => Detail of selected param
    shift
    DETAIL="$@"
    # Display and read reponse
    read -p "${DETAIL}: " VALUE
    # Delete line if exist, and write in conf file
    sed -i "/$PARAM/d" $CONF_FILE
    # Write param name and value in conf file
    echo "${PARAM}='$VALUE'" >> $CONF_FILE
    # Set to true to reload this script
    RELOAD_PARAMATER=true
}

# List names of variables's parameter and their description
LIST_PARAM="
UNATTENDED_EMAIL Unattended email alert
GIT_USERNAME Git username
GIT_EMAIL Git email
SSH_EMAIL_ALERT alert email for new ssh connexion
"

IFS=$'\n'

# Test if conf file exist
if [ -f $CONF_FILE ] ;then
    # If conf file exist, load it and test if each variable is defined
    . $CONF_FILE
        for DETAIL_PARAM in $LIST_PARAM ;do
            PARAM=${DETAIL_PARAM%% *} # Delete all after first space
            DETAIL=${DETAIL_PARAM#* } # Delete all before first space
            # If variable is not defined, or if variable is empty, do request it
            if [ -z ${!PARAM} ] ;then
                func_read_param $PARAM $DETAIL
            fi
        done
else
    # Conf file doesn't exist
    touch $CONF_FILE # Create empty conf file
    # For all variable, do request it
    for DETAIL_PARAM in $LIST_PARAM ;do
        PARAM=${DETAIL_PARAM%% *} # Delete all after first space
        DETAIL=${DETAIL_PARAM#* } # Delete all before first space
        func_read_param $PARAM $DETAIL
    done
fi

# If reload is required
if $RELOAD_PARAMATER ; then
    $0 # Restart this script
    exit 0
fi

################################################################################

# Update packages list
apt-get update --quiet=2

# Install packages
apt-get -y install      \
    git                 \
    tig                 \
    bc                  \
    procps              \
    bash-completion     \
    bsdmainutils        \
    unattended-upgrades \
    apt-listchanges     \
    bind9-host          \
    bsd-mailx           \
    logrotate           > /dev/null

################################################################################

# Configure auto updates
UNA_FILE="/etc/apt/apt.conf.d/50unattended-upgrades"
# email setting
if ! grep -q '^Unattended-Upgrade::Mail ' $UNA_FILE ; then
    echo "Unattended-Upgrade::Mail \"$UNATTENDED_EMAIL\";" >> $UNA_FILE
else
    sed -i "s/Unattended-Upgrade::Mail .*/Unattended-Upgrade::Mail '$UNATTENDED_EMAIL';/" $UNA_FILE
fi
# MailOnlyOnError
if ! grep -q '^Unattended-Upgrade::MailOnlyOnError ' $UNA_FILE ; then
    echo "Unattended-Upgrade::MailOnlyOnError \"true\";" >> $UNA_FILE
else
    sed -i "s/Unattended-Upgrade::MailOnlyOnError .*/Unattended-Upgrade::MailOnlyOnError \"true\";/" $UNA_FILE
fi

################################################################################

# Set git global config
git config --global user.name "$GIT_USERNAME"
git config --global user.email "$GIT_EMAIL"

## Symbolic links
# Bashrc
ln -sf $GIT_DIR/root/.bashrc /root/
# Profile
ln -sf $GIT_DIR/root/.profile /root/
mkdir -p /root/.profile.d
ln -sf $GIT_DIR/root/.profile.d/00_functions.conf       /root/.profile.d/
ln -sf $GIT_DIR/root/.profile.d/00_systemd_journal.conf /root/.profile.d/
ln -sf $GIT_DIR/root/.profile.d/01_stats_connexion.conf /root/.profile.d/
# vimrc
ln -sf $GIT_DIR/root/.vimrc /root/

################################################################################

## Tune logrotate

# Force rotate if is launched by cron
# change : /usr/sbin/logrotate /etc/logrotate.conf
# by     : /usr/sbin/logrotate -f /etc/logrotate.conf
sed -i 's#\(/usr/sbin/logrotate\).*\(/etc/logrotate.conf\)#\1 -f \2#' /etc/cron.daily/logrotate

# Disable delaycompress
sed -i 's/.*delaycompress/#&/' /etc/logrotate.d/*

################################################################################

# Disable IPv6
if $DISABLE_IPv6 ; then
    cat << EOF > /etc/sysctl.d/98-disable-ipv6.conf
# Disable ipv6 on all connexion
net.ipv6.conf.all.disable_ipv6 = 1
EOF
    # Disable IPv6 now
    systemctl restart systemd-sysctl.service
fi

################################################################################

# alert by email for new ssh connexion
cp $GIT_DIR/etc/ssh/sshrc /etc/ssh/sshrc
sed -i "s/__ALERT_EMAIL__/$SSH_EMAIL_ALERT/" /etc/ssh/sshrc

################################################################################

# Time Zone configuration
# see 'parameters.conf' to edit value
busctl call org.freedesktop.timedate1 /org/freedesktop/timedate1 org.freedesktop.timedate1 SetTimezone "sb" "$TimeZone" true
