# If .bashrc.d directory exist, load .conf files
if [[ -f ~/.bashrc.d/*.conf ]]; then
    for bashrc_d_file in ~/.bashrc.d/*.conf ; do
        . $bashrc_d_file
    done
fi

# alias
export LS_OPTIONS='--color=auto'
eval "`dircolors`"
alias ls='ls $LS_OPTIONS -h'
alias ll='ls $LS_OPTIONS -lh'
alias l='ls $LS_OPTIONS -lAh'

# Some more alias to avoid making mistakes:
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Color grep
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Git :
alias ggb="cd /srv/git/basic_config_debian"
alias gs="git status"
alias gd="git diff"
alias ga="git add"
alias gb="git branch"
alias gc="git commit"
alias gca="git commit -v -a"
alias gcam="git commit --amend -v"
alias gp="git push origin master"
alias gl="git log"
alias gll="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

# Network
alias ipa='ip -color addr show'
alias ip4a='ip -4 -color addr show'
alias ip6a='ip -6 -color addr show'

# PS1
case $(systemd-detect-virt) in
    "qemu"|"kvm")  PS1_COLOR="34m";; # Blue
    "lxc")   PS1_COLOR="33m";; # Orange
    *)       PS1_COLOR="32m";; # Green
esac
export PS1="\[\033]0;\u@\h - \w\007\]\[\e[$PS1_COLOR\]root@\h\[\e[0m\] [\[\e[0m\]\t\[\e[0m\]][\[\e[34m\]\w\[\e[0m\]]$ "

# History format
HISTTIMEFORMAT='%Y-%m-%d %H:%M:%S '

