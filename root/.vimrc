" Activation de la colorisation syntaxique :
syntax on
"
" Gestion de la casse pour les recherches «/ »
"« set ignorecase » for case-insensitive searching
"« set smartcase » which will automatically switch to a case-sensitive search if you use any capital letters
"set smartcase
set ignorecase
"
" thèmes de couleurs :
colorscheme koehler
"colorscheme pablo
"
" (et) activer le remplacement des tabulations par des espaces :
set expandtab     " (et) expand tabs to spaces (use :retab to redo entire file)
"
" (ts) nombre d'espace qui seront générés lors de l'appuie de la touche <tab> :
set tabstop=4     " (ts) width (in spaces) that a <tab> is displayed as
"
" (sw) nombre d'espaces utilisés pour l'auto indentation :
set shiftwidth=4  " (sw) width (in spaces) used in each step of autoindent (aswell as << and >>)
"
" Permet de prendre en compte les lignes du type suivant pour surcharger le conf vim
" # vim: syntax=sh ts=4 sw=4 sts=4
" http://vim.wikia.com/wiki/Modeline_magic
set modeline
"
" mettre surbrillance les résultats de la recherche
set hlsearch
"
" Permet de retourner a la dernière ligne lue dans un fichier
if has("autocmd")
    au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

