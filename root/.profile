# ~/.profile: executed by Bourne-compatible login shells.
if [ "$BASH" ]; then
    if [ -f ~/.bashrc ]; then
        . ~/.bashrc
    fi
fi

mesg n

################################################################################
# My conf

# if .profile.d directory exist, load .conf files
#if [ -f ~/.profile.d/*.conf ] ;then
if [ -d ~/.profile.d/ ] ;then
    for profile_d_file in ~/.profile.d/*.conf
    do
        . $profile_d_file
    done
fi

